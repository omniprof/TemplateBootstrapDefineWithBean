package com.kfwebstandard.action;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author kfogel
 */
@Named
@RequestScoped
public class NextPageAction {

    public String doNextPage() {
        return "success";
    }

}
