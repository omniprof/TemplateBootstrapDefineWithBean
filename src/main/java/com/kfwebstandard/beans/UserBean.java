package com.kfwebstandard.beans;

import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 *
 * @author kfogel
 */
@Named
@RequestScoped
public class UserBean {

    private String name;
    private String password;
    private static final Logger LOG = Logger.getLogger(UserBean.class.getName());

    public String getName() {
        LOG.info("getName");
        return name;
    }

    public void setName(String name) {
        LOG.info("setName");
        this.name = name;
    }

    public String getPassword() {
        LOG.info("getPassword");
        return password;
    }

    public void setPassword(String password) {
        LOG.info("setPassword");
        this.password = password;
    }

}
