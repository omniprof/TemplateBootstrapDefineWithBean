package com.kfwebstandard.selector;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

/**
 * This class acts as a selector for a page selector There is no logic, it is
 * just a demonstration
 *
 * @author Ken
 */
@Named
@RequestScoped
public class Selector {

    private final String select1;
    private final String select2;

    public Selector() {
        select1 = "/WEB-INF/content/content1.xhtml";
        select2 = "/WEB-INF/content/content2.xhtml";
    }

    public String getSelect1() {
        return select1;
    }

    public String getSelect2() {
        return select2;
    }

}
